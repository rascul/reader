use std::fmt::{Display, Formatter, Result as FmtResult};
use std::io::Error as IoError;

use handlebars_iron::SourceError as HandlebarsIronSourceError;
use iron::error::IronError;
use native_tls::Error as TlsError;
use postgres::error::Error as PostgresError;
use r2d2::Error as R2d2Error;
use reqwest::Error as ReqwestError;
use rss::Error as RssError;
use toml::de::Error as TomlDeError;

pub type ReaderResult<T> = Result<T, ReaderError>;

#[derive(Debug)]
pub enum ReaderError {
	HandlebarsIronSource(HandlebarsIronSourceError),
	Io(IoError),
	Iron(IronError),
	Postgres(PostgresError),
	R2d2(R2d2Error),
	Reqwest(ReqwestError),
	Rss(RssError),
	Tls(TlsError),
	TomlDe(TomlDeError),
	Custom(String),
}

impl From<HandlebarsIronSourceError> for ReaderError {
	fn from(e: HandlebarsIronSourceError) -> ReaderError {
		ReaderError::HandlebarsIronSource(e)
	}
}

impl From<IoError> for ReaderError {
	fn from(e: IoError) -> ReaderError {
		ReaderError::Io(e)
	}
}

impl From<IronError> for ReaderError {
	fn from(e: IronError) -> ReaderError {
		ReaderError::Iron(e)
	}
}

impl From<PostgresError> for ReaderError {
	fn from(e: PostgresError) -> ReaderError {
		ReaderError::Postgres(e)
	}
}

impl From<R2d2Error> for ReaderError {
	fn from(e: R2d2Error) -> ReaderError {
		ReaderError::R2d2(e)
	}
}

impl From<RssError> for ReaderError {
	fn from(e: RssError) -> ReaderError {
		ReaderError::Rss(e)
	}
}

impl From<ReqwestError> for ReaderError {
	fn from(e: ReqwestError) -> ReaderError {
		ReaderError::Reqwest(e)
	}
}

impl From<TlsError> for ReaderError {
	fn from(e: TlsError) -> ReaderError {
		ReaderError::Tls(e)
	}
}

impl From<TomlDeError> for ReaderError {
	fn from(e: TomlDeError) -> ReaderError {
		ReaderError::TomlDe(e)
	}
}

impl<'a> From<&'a str> for ReaderError {
	fn from(e: &'a str) -> ReaderError {
		ReaderError::Custom(e.to_string())
	}
}

impl From<String> for ReaderError {
	fn from(e: String) -> ReaderError {
		ReaderError::Custom(e)
	}
}

impl Display for ReaderError {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		match *self {
			ReaderError::HandlebarsIronSource(ref e) => Display::fmt(e, f),
			ReaderError::Io(ref e) => Display::fmt(e, f),
			ReaderError::Iron(ref e) => Display::fmt(e, f),
			ReaderError::Postgres(ref e) => Display::fmt(e, f),
			ReaderError::R2d2(ref e) => Display::fmt(e, f),
			ReaderError::Reqwest(ref e) => Display::fmt(e, f),
			ReaderError::Rss(ref e) => Display::fmt(e, f),
			ReaderError::Tls(ref e) => Display::fmt(e, f),
			ReaderError::TomlDe(ref e) => Display::fmt(e, f),
			ReaderError::Custom(ref e) => Display::fmt(e, f),
		}
	}
}
