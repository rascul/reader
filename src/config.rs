use std::fs;
use std::io::Read;
use std::path::Path;

use typemap::Key;
use toml;

use error::ReaderResult;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Config {
	pub site: Site,
	pub database: Database,
	pub paths: Paths,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Site {
	pub url: String,
	pub serve: String,
	pub secret: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Database {
	pub url: String,
	pub prefix: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Paths {
	pub templates: String,
	pub assets: String,
	pub data: String,
}

impl Key for Config {
	type Value = Config;
}

impl Config {
	pub fn load<T: AsRef<Path>>(path: T) -> ReaderResult<Config> {
		let mut file = fs::File::open(path.as_ref())?;
		let mut buf = String::new();
		file.read_to_string(&mut buf)?;
		Ok(toml::from_str(&buf)?)
	}
}
