use std::io::{Read, Write, stdin, stdout};

use config::Config;
use db::pgpool::PgPool;
use error::ReaderResult;

pub fn init(config: &Config, pgpool: &PgPool) -> ReaderResult<()> {
	println!("\nWARNING WARNING WARNING WARNING WARNING WARNING\n");
	println!(
		"This will delete the '{0}feeds' and '{0}entries' tables if they exist,",
		&config.database.prefix
	);
	println!("then recreate them. This has the effect of losing any data currently");
	println!("in those tables. Do not run this against a database with data unless");
	println!("any data which may be in those tables has been backed up or you are");
	println!("willing to lose it forever.");
	println!("\nPress ENTER to continue or CTRL+C to cancel.\n");

	stdout().flush()?;
	let _ = stdin().read(&mut [0u8])?;

	let conn = pgpool.get()?;

	println!("Dropping index '{}entries_pubdate_index'", &config.database.prefix);
	conn.execute(&format!(
		"drop index if exists {}entries_pubdate_index",
		&config.database.prefix
	), &[])?;

	println!("Dropping index '{}entries_feed_index'", &config.database.prefix);
	conn.execute(&format!(
		"drop index if exists {}entries_feed_index",
		&config.database.prefix
	), &[])?;

	println!("Dropping index '{}entries_id_index'", &config.database.prefix);
	conn.execute(&format!(
		"drop index if exists {}entries_id_index",
		&config.database.prefix
	), &[])?;

	println!("Dropping table '{}entries'", &config.database.prefix);
	conn.execute(&format!(
		"drop table if exists {}entries",
		&config.database.prefix
	), &[])?;

	println!("Dropping index '{}feeds_category_index'", &config.database.prefix);
	conn.execute(&format!(
		"drop index if exists {}feeds_category_index",
		&config.database.prefix
	), &[])?;

	println!("Dropping index '{}feeds_id_index'", &config.database.prefix);
	conn.execute(&format!(
		"drop index if exists {}feeds_id_index",
		&config.database.prefix
	), &[])?;

	println!("Dropping table '{}feeds'", &config.database.prefix);
	conn.execute(&format!(
		"drop table if exists {}feeds",
		&config.database.prefix
	), &[])?;

	println!("Dropping table '{}categories'", &config.database.prefix);
	conn.execute(&format!(
		"drop table if exists {}categories",
		&config.database.prefix
	), &[])?;

	println!("Creating table '{}categories'", &config.database.prefix);
	conn.execute(&format!(
		"create table {}categories (
			name text primary key
		);",
		&config.database.prefix),
		&[]
	)?;

	println!("Creating table '{}feeds'", &config.database.prefix);
	conn.execute(&format!(
		"create table {0}feeds (
			id bigserial primary key,
			title text not null,
			link text not null unique,
			description text not null,
			color text not null,
			category text references {0}categories(name)
		);",
		&config.database.prefix),
		&[]
	)?;

	println!("Creating index '{}feeds_id_index'", &config.database.prefix);
	conn.execute(&format!(
		"create index {0}feeds_id_index on {0}feeds(id);",
		&config.database.prefix
	), &[])?;

	println!("Creating index '{}feeds_category_index'", &config.database.prefix);
	conn.execute(&format!(
		"create index {0}feeds_category_index on {0}feeds(category);",
		&config.database.prefix
	), &[])?;

	println!("Creating table '{}entries'", &config.database.prefix);
	conn.execute(&format!(
		"create table {0}entries (
			id bigserial primary key,
			feed bigserial references {0}feeds(id),
			title text,
			link text,
			description text,
			comments text,
			pubdate timestamp with time zone not null,
			hash text not null unique,
			read bool,
			starred bool
		);",
		&config.database.prefix),
		&[]
	)?;

	println!("Creating index '{}entries_id_index'", &config.database.prefix);
	conn.execute(&format!(
		"create index {0}entries_id_index on {0}entries(id);",
		&config.database.prefix
	), &[])?;

	println!("Creating index '{}entries_feed_index'", &config.database.prefix);
	conn.execute(&format!(
		"create index {0}entries_feed_index on {0}entries(feed);",
		&config.database.prefix
	), &[])?;

	println!("Creating index '{}entries_pubdate_index'", &config.database.prefix);
	conn.execute(&format!(
		"create index {0}entries_pubdate_index on {0}entries(pubdate);",
		&config.database.prefix
	), &[])?;

	Ok(())
}
