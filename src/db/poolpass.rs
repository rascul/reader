use typemap::Key;

use db::pgpool::PgPool;

pub struct PoolPass;

impl Key for PoolPass {
	type Value = PgPool;
}
