use r2d2::{Pool, PooledConnection};
use r2d2_postgres::{PostgresConnectionManager, TlsMode};
use postgres::tls::native_tls::NativeTls;

use error::ReaderResult;

#[derive(Clone)]
pub struct PgPool {
	pub pool: Pool<PostgresConnectionManager>
}

impl PgPool {
	pub fn new<T: AsRef<str>>(url: T) -> ReaderResult<PgPool> {
		Ok(PgPool {
			pool: Pool::new(PostgresConnectionManager::new(
				url.as_ref(),
				TlsMode::Prefer(Box::new(NativeTls::new()?))
			)?)?
		})
	}

	pub fn get(&self) -> ReaderResult<PooledConnection<PostgresConnectionManager>> {
		Ok(self.pool.get()?)
	}
}
