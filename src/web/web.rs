use iron::prelude::{Chain, Iron};
use mount::Mount;
use persistent::Read;

use config::Config;
use db::{PgPool, PoolPass};
use error::ReaderResult;
use routes;
use session;
use templates;

pub fn run(config: &Config, pgpool: PgPool) -> ReaderResult<()> {
	let session = session::setup(&config.site.secret);
	let router = routes::build();
	let mut mount = Mount::new();
	mount.mount("/", router);

	let mut chain = Chain::new(mount);
	chain.link(Read::<Config>::both(config.clone()));
	chain.link(Read::<PoolPass>::both(pgpool));
	chain.link_around(session.unwrap());
	chain.link_after(templates::load("templates")?);

	match Iron::new(chain).http(&config.site.serve) {
		Ok(_) => println!("listening on {}", &config.site.serve),
		Err(e) => return Err(format!("{:?}", e).into()),
	}

	Ok(())
}
