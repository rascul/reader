use std::sync::Arc;

use handlebars::no_escape;
use handlebars_iron::{DirectorySource, HandlebarsEngine};
use handlebars_iron::Watchable;

use error::ReaderResult;

/*#[cfg(not(feature = "watch"))]
pub fn load<T: AsRef<str>>(path: T) -> ReaderResult<HandlebarsEngine> {
	let mut handlebars_engine = HandlebarsEngine::new();
	handlebars_engine.handlebars_mut().register_escape_fn(no_escape);
	handlebars_engine.add(Box::new(DirectorySource::new(path.as_ref(), ".hbs")));
	handlebars_engine.reload()?;
	Ok(handlebars_engine)
}*/

pub fn load<T: AsRef<str>>(path: T) -> ReaderResult<Arc<HandlebarsEngine>> {
	let mut handlebars_engine = HandlebarsEngine::new();
	handlebars_engine.handlebars_mut().register_escape_fn(no_escape);
	handlebars_engine.add(Box::new(DirectorySource::new(path.as_ref(), ".hbs")));
	handlebars_engine.reload()?;

	let handlebars_ref = Arc::new(handlebars_engine);
	handlebars_ref.watch(path.as_ref());

	Ok(handlebars_ref)
}
