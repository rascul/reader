use std::sync::Arc;

use handlebars_iron::handlebars::to_json;
use handlebars_iron::Template;
use iron::prelude::*;
use iron::status;
use persistent::Read;
use router::Router;
use serde_json::value::Map;

use config::Config;
use db::PoolPass;
use error::ReaderResult;
use refresh;

macro_rules! route {
	($name:ident) => (
		fn $name() {
			println!("hi there");
		}
	)
}

		//|req: Request| -> IronResult<Response> {
			//fn $name(req: &mut Request) -> IronResult<Response> {
			//fn somethung(req: &mut Request) -> ReaderResult<Response> {
			//	$block
			//}

			//println!("somethung: {}", somethung());

			/*match somethung(req) {
				Ok(res) => Ok(res),
				Err(e) => {
					let mut res = Response::new();
					//let config = req.get::<Read<Config>>().unwrap();
					//let mut data = Map::new();
					//data.insert("config".to_string(), to_json(&config));
					//data.insert("error".to_string(), to_json(format!("{:?}", e)));
					//res.set_mut(Template::new("error", data)).set_mut(status::InternalServerError);
					Ok(res)
				},
			}*/
		//}
	//})
//}





pub fn build() -> Router {
	let mut router = Router::new();

	router.get("/", index, "index");
	/*router.get("/feeds", route!(feeds, {
		//let config = req.get::<Read<Config>>().unwrap();
		println!("woo");
		let mut res = Response::new();
		res.set_mut(Template::with("hi", ())).set_mut(status::Ok);
		Ok(res)
	}), "feeds");*/
	router.get("/refresh", refresh, "refresh");
	/*router.get("/poo", |req: Request| -> IronResult<Response> {
		Ok(Response::new())
	}, "poo");*/
	router.get("/poo", poo, "poo");
	router
}

fn index(_req: &mut Request) -> IronResult<Response> {
	let mut res = Response::new();
	res.set_mut(Template::new("index", ())).set_mut(status::Ok);
	Ok(res)
}

fn refresh(req: &mut Request) -> IronResult<Response> {
	let config = req.get::<Read<Config>>().unwrap();
	let pgpool = req.get::<Read<PoolPass>>().unwrap();

	let success = refresh::refresh(&config, &pgpool).is_ok();

	let mut data = Map::new();
	data.insert("config".to_string(), to_json(&config));
	data.insert("success".to_string(), to_json(&success));

	let mut res = Response::new();
	res.set_mut(Template::new("refresh", data)).set_mut(status::Ok);

	Ok(res)
}

fn poo(req: &mut Request) -> IronResult<Response> {
	route!(hi);
	hi();
	Ok(Response::new())
}

/*fn run_feeds(req: &mut Request) -> ReaderResult<(Arc<Config>, Response)> {
	let config = req.get::<Read<Config>>().unwrap();
	let pgpool = req.get::<Read<PoolPass>>().unwrap();
	println!("req: {:?}", req);
	let mut res = Response::new();
	Ok((config, res))
}


fn feeds(req: &mut Request) -> IronResult<Response> {
	match run_feeds(req) {
		Ok((config, res)) => Ok(res),
		Err(e) => Ok(Response::new()),
	}
}*/





/*fn feed(req: &mut Request) -> IronResult<Response> {
	let mut res = Response::new();

	if let Ok(map) = req.get_ref::<params::Params>() {
		if let Some(&params::Value::String(ref url)) = map.get("url") {
			let channel = feed::read(url);
			res.set_mut(Template::new("feed", channel)).set_mut(status::Ok);
		} else {
			let mut error = HashMap::new();
			error.insert("code", "400");
			error.insert("description", "Bad Request: No url specified");
		}
	} else {
		let mut error = HashMap::new();
		error.insert("code", "400");
		error.insert("description", "Bad Request: Invalid Request");
		res.set_mut(Template::new("error", error)).set_mut(status::BadRequest);
	}

	Ok(res)
}

fn hn(_req: &mut Request) -> IronResult<Response> {
	let channel = feed::read("https://news.ycombinator.com/rss");

	let mut res = Response::new();
	res.set_mut(Template::new("feed", channel)).set_mut(status::Ok);
	Ok(res)
}*/
