use secure_session::middleware::{SessionMiddleware, SessionConfig};
use secure_session::session::ChaCha20Poly1305SessionManager;

use typemap::Key;

use error::ReaderResult;

#[derive(Deserialize, Serialize)]
pub struct Session {
	message: String,
}

#[derive(Deserialize, Serialize)]
pub struct SessionKey {}

impl Key for SessionKey {
	type Value = Session;
}

pub type ReaderSession = SessionMiddleware<Session, SessionKey, ChaCha20Poly1305SessionManager<Session>>;

//pub fn setup(key: &'static [u8; 32]) -> SessionMiddleware<
//pub fn setup<T: AsRef<str>>(key: T) -> ReaderResult<SessionMiddleware<
//	Session,
//	SessionKey,
//	ChaCha20Poly1305SessionManager<Session>
//>> {

impl Session {
	pub fn setup<T: AsRef<str>>(key: T) -> ReaderResult<Session> {
		let key = key.as_ref();
		if key.len() != 32 {
			return Err("Invalid key size. Must be 32 characters.".into());
		}

		let mut key_array: [u8; 32] = [0; 32];
		key_array[..32].clone_from_slice(&key.as_bytes()[..32]);

		//let session_manager = ChaCha20Poly1305SessionManager::<Session>::from_key(key_array);
		//let session_config = SessionConfig::default();
		//Ok(SessionMiddleware::new(session_manager, session_config))

		Ok(ReaderSession::new(
			ChaCha20Poly1305SessionManager::<Session>::from_key(key_array),
			SessionConfig::default()
		))
	}
}
