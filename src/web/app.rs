use iron::prelude::{Chain, Iron};
use mount::Mount;
use persistent::Read;

use config::Config;
use db::{PgPool, PoolPass};
use error::ReaderResult;
use super::routes;
use web::session::Session;
use web::templates;

pub struct Reader {
	pub config: Config,
	session: Session,
	chain: Chain,
}

impl Reader {
	pub fn new(config: Config, pgpool: PgPool) -> Reader {
		let session: Session = Session::setup(&config.site.secret).unwrap();
		let router = routes::build();
		let mut mount = Mount::new();
		mount.mount("/", router);

		let mut chain = Chain::new(mount);
		chain.link(Read::<Config>::both(config.clone()));
		chain.link(Read::<PoolPass>::both(pgpool));
		chain.link_around(session);
		chain.link_after(templates::load("templates").unwrap());

		Reader {
			config: config,
			session: session,
			chain: chain
		}
	}

	pub fn run(&self) -> ReaderResult<()> {
		match Iron::new(&self.chain).http(&self.config.site.serve) {
			Ok(_) => println!("listening on {}", &self.config.site.serve),
			Err(e) => return Err(format!("{:?}", e).into()),
		}

		Ok(())
	}
}
