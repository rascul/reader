use config::Config;
use db::PgPool;
use web::Reader;

pub fn run(config: Config, pgpool: PgPool) {
	let reader = Reader::new(config, pgpool);
	reader.run().unwrap();
}
