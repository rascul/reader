use config::Config;
use db::PgPool;
use error::ReaderResult;
use types::Feed;

pub fn refresh(config: &Config, pgpool: &PgPool) -> ReaderResult<()> {
	let conn = pgpool.get()?;
	let query = format!("select * from {}feeds;", &config.database.prefix);

	for row in conn.query(&query, &[])?.iter() {
		let mut feed = Feed::from(row);
		if let Err(e) = feed.fetch_entries() {
			println!("Error fetching entries: {:?}", e);
		}

		for entry in feed.entries {
			conn.execute(&format!(
				"insert into {}entries (
					feed, title, link, description, comments, pubdate, hash, read, starred
				) values (
					$1, $2, $3, $4, $5, $6, $7, false, false
				) on conflict do nothing;",
				&config.database.prefix
			), &[
				&feed.id,
				&entry.title,
				&entry.link,
				&entry.description,
				&entry.comments,
				&entry.pubdate,
				&entry.hash
			])?;
		}
	}

	Ok(())
}
