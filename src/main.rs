extern crate chrono;
extern crate clap;
extern crate handlebars;
extern crate handlebars_iron;
extern crate iron;
extern crate mount;
extern crate native_tls;
extern crate params;
extern crate persistent;
extern crate postgres;
extern crate r2d2;
extern crate r2d2_postgres;
extern crate reqwest;
extern crate router;
extern crate rss;
extern crate secure_session;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate toml;
extern crate typemap;

mod config;
mod db;
mod error;
mod refresh;
mod types;
mod web;

use clap::{App, Arg, SubCommand};

use config::Config;
use error::ReaderResult;
use db::PgPool;

fn main() {
	if let Err(e) = check_args() {
		println!("Error: {:?}", e);
		std::process::exit(1);
	}
}

fn check_args() -> ReaderResult<()> {
	let args = App::new("reader")
		.version("0.1")
		.about("Web based news feed reader")
		.arg(Arg::with_name("config")
			.short("c")
			.long("config")
			.value_name("FILE")
			.help("Path to config file")
			.takes_value(true)
		).subcommand(SubCommand::with_name("refresh")
			.about("Refresh feeds")
		).subcommand(SubCommand::with_name("initdb")
			.about("Initialize database (WARNING: MAY DESTROY DATA)")
		).get_matches();

	//let config_file = args.value_of("config").unwrap_or("reader.toml");
	//let config = Config::load(config_file)?;

	let config = Config::load(args.value_of("config").unwrap_or("reader.toml")).unwrap();
	let pgpool = PgPool::new(&config.database.url)?;

	match args.subcommand() {
		("refresh", Some(_)) => refresh::refresh(&config, &pgpool),
		("initdb", Some(_)) => db::init(&config, &pgpool),
		_ => Ok(web::run(config, pgpool)),
	}
}
