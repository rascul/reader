use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use chrono::{DateTime, Utc};
use postgres::Result as PostgresResult;
use postgres::rows::Row;
use rss::Item;

#[derive(Debug)]
pub struct Entry {
	pub id: Option<i64>,
	pub feed: Option<i64>,
	pub title: Option<String>,
	pub link: Option<String>,
	pub description: Option<String>,
	pub comments: Option<String>,
	pub pubdate: DateTime<Utc>,
	pub hash: String,
	pub read: bool,
	pub starred: bool,
}

impl Entry {
	fn compute_hash(&mut self) {
		let mut hasher = DefaultHasher::new();
		self.hash(&mut hasher);
		self.hash = hasher.finish().to_string();
	}
}


impl Hash for Entry {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.feed.hash(state);
		self.link.hash(state);
		self.pubdate.hash(state);
	}
}

impl<'a> From<&'a Item> for Entry {
	fn from(item: &'a Item) -> Entry {
		let mut e = Entry {
			id: None,
			feed: None,
			title: option_str_to_string(item.title()),
			link: option_str_to_string(item.link()),
			description: option_str_to_string(item.description()),
			comments: option_str_to_string(item.comments()),
			pubdate: parse_pubdate(item.pub_date()),
			hash: "".to_string(),
			read: false,
			starred: false,
		};

		e.compute_hash();
		e
	}
}

fn parse_pubdate(pubdate: Option<&str>) -> DateTime<Utc> {
	if let Some(pd) = pubdate {
		match DateTime::parse_from_rfc2822(&pd) {
			Ok(dt) => return dt.with_timezone(&Utc),
			Err(e) => println!("Error parsing pubdate: {:?}", e),
		}
	}
	Utc::now()
}

impl<'a> From<Row<'a>> for Entry {
	fn from(row: Row<'a>) -> Entry {
		Entry {
			id: row.get("id"),
			feed: row.get("feed"),
			title: check_option(row.get_opt("title")),
			link: check_option(row.get_opt("link")),
			description: check_option(row.get_opt("description")),
			comments: check_option(row.get_opt("comments")),
			pubdate: row.get("pubdate"),
			hash: row.get("hash"),
			read: row.get("read"),
			starred:row.get("starred"),
		}
	}
}

fn check_option<T>(o: Option<PostgresResult<T>>) -> Option<T> {
	match o {
		Some(Ok(v)) => Some(v),
		Some(Err(e)) => {
			println!("Warning: {:?}", e);
			None
		},
		None => None,
	}
}

fn option_str_to_string<'a>(o: Option<&'a str>) -> Option<String> {
	match o {
		Some(v) => Some(v.to_string()),
		None => None,
	}
}
