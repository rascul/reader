use std::str::FromStr;

use postgres::rows::Row;
use reqwest::get;
use rss::Channel;

use types::Entry;
use error::ReaderResult;

#[derive(Debug)]
pub struct Feed {
	pub id: i64,
	pub title: String,
	pub link: String,
	pub description: String,
	pub color: String,
	pub category: Option<String>,
	pub entries: Vec<Entry>,
}

impl Feed {
	pub fn fetch_entries(&mut self) -> ReaderResult<()> {
		let mut res = get(&self.link)?;
		if !res.status().is_success() {
			return Err(format!("Error: {} from {}", &res.status(), &self.link).into());
		}

		let body = res.text()?;
		let channel = Channel::from_str(&body)?;

		for item in channel.items() {
			self.entries.push(Entry::from(item));
		}

		Ok(())
	}
}

impl<'a> From<Row<'a>> for Feed {
	fn from(row: Row<'a>) -> Feed {
		Feed {
			id: row.get("id"),
			title: row.get("title"),
			link: row.get("link"),
			description: row.get("description"),
			color: row.get("color"),
			category: row.get("category"),
			entries: Vec::new(),
		}
	}
}
